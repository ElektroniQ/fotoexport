from tkinter import *
import QuestionField


def Restart_Callback():
    pass

root = Tk()
root.title("FotoExpert")
root.configure(background = "#5894fc")
root.geometry("1000x500")

restartButton = Button(root, text="Zacznij od nowa", command=Restart_Callback)
restartButton.grid(row = 0, column=0, sticky="we",padx=50, pady=30)

helpButton = Button(root, text="Pomoc", command=Restart_Callback)
helpButton.grid(row = 0, column=1, sticky="we", pady=30)

aboutButton = Button(root, text="O programie", command=Restart_Callback)
aboutButton.grid(row = 0, column=2, sticky="e",padx=50, pady=30)

mainField = QuestionField.QuestionField(root)
mainField.AddQuestion('pyt1', 'Pytanie 1', 'Tak', 'Nie', 2)
mainField.AddQuestion('pyt2', 'Pytanie 2', 'Tak1', 'Nie1', 2)
mainField.AddQuestion('pyt3', 'Pytanie 3', 'Tak2', 'Nie2', 5)
mainField.Init()
mainField.GetObject().grid(row = 1, column = 0, columnspan = 3, sticky = "e", padx=50, pady=30)


def GetResult_Callback():
    for i in mainField.questions:
        print(i)


ExitButton = Button(root, text="Zakończ", command=GetResult_Callback)
ExitButton.grid(row = 2, column=2, sticky="e",padx=50, pady=30)

root.mainloop()

